nodeio = require 'node.io'
urlParse = require 'url'
http = require 'http'
sys = require 'sys'
mysql = require 'db-mysql'
connection = null
dbVars = 
  hostname: 'HOST'
  user: 'USER'
  password: 'PASS'
  database: 'QA_DB_NAME'
db = new mysql.Database(dbVars)
  .on 'ready', (server) ->
    connection = @
  .connect (error)->
    if error?
      console.log 'Error connecting to Database: ' + error 
      #process.exit 1
http.Agent.defaultMaxSockets = 500
ticks = 0

timer = () ->
  ticks++
  console.log 'Elapsed: ' + (ticks * 30) + ' seconds'
setInterval(timer, 30000)

# This is not ideal, and probably should not be here,
# but I don't want to lose my place in the scraper.
process.on 'uncaughtException', (err) ->
  console.log 'UNCAUGHT ERROR!: ' + err;


baseUrl = urlParse.parse 'http://www.EXAMPLE.com/'
first = true;
linkQueue = ['http://www.EXAMPLE.com']
parsedLinks = ['http://www.EXAMPLE.com/cgi-bin/cart', 'http://www.EXAMPLE.com/portal/', 'http://www.EXAMPLE.com/team/']
badImages = []

addLink = (href, IOJob) -> 
  if href in linkQueue or href in parsedLinks
    passed =
      'new': false
      href: href
    passed
  else 
    linkQueue.push href
    IOJob.add href
    passed = 
      'new': true
      href: href
    passed

getLink = (a, url) ->  
  parsedUrl = urlParse.parse a.attribs.href
  href = false
  if parsedUrl.hasOwnProperty 'host'
    if parsedUrl.host is baseUrl.host
      href = parsedUrl.href
    else 
      href = false
  else if parsedUrl.protocol is 'javascript:'
    href = url + '?itemsperpage=10000'
  else #probably an unidentified http ex '/abc.html'
    href = urlParse.resolve baseUrl.href, parsedUrl.pathname + if parsedUrl.hasOwnProperty 'search' then parsedUrl.search else ''
  
  unless href is false
    #res = addLink href
    #if res.new then IOJob.add href
    return href
  else 
    return false


DB =
  updatePage: (url, response) ->
    id = null
    idfn = (err, res)->
      if err?
        return console.log err
      id =  res.id
    q = connection.query('INSERT INTO Pages(url, `date`, response) VALUES(?, CURDATE(), ?) ON DUPLICATE KEY UPDATE url=?, `date`=CURDATE(), response=?', [url, response, url, response])  
    q.execute idfn, {'async': false}
    return id
      
  updateProduct: (name, id, dbid) ->
    year = null
    category = null

    if id?
      itemSplit = id.split '-'
      year = itemSplit[0].match(/.+?([0-9]{1,2}zz)$/)
      if year?
        year = year[1].replace 'zz', ''
      else
        year = null
      itemSplit.splice 0, 1
      itemSplit.splice 0, 1
      category = itemSplit.join " "
    
    connection.query('INSERT INTO Products(page_id, name, class, item_id, year) VALUES(?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE item_id=?', [dbid, name, category, id, year, id])
      .execute (err, res) ->
        if err?
          if err.hasOwnProperty 'message'
            console.log 'Product DB write failed: ' + err

  updateImage: (img, code, dbid) ->
    connection.query('INSERT INTO Images(page_id, url, response) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE response=?', [dbid, img, code, code])
      .execute (err, res) ->
        if err?
          if err.hasOwnProperty 'message'
            console.log 'Image DB write failed: ' + err

verifyImages = (images, url, dbid) ->
  images.each (img) ->
    imgUrl = urlParse.parse img.attribs.src
    if not imgUrl.hasOwnProperty 'host'
      imgUrl.host = baseUrl.host
    req = http.get {host: imgUrl.host, port: 80, path: imgUrl.pathname}, (res) ->
      res.on 'error', (err) -> 
        console.log 'Image response Error:' + err.message
      imgHref = imgUrl.host + imgUrl.pathname
      if res.statusCode is 200
        
      else
        if imgHref not in badImages
          badImages.push imgHref
          if badImages.length % 10 is 0
            console.log 'Bad Images: ' + badImages.length
        #console.log '4. ' + res.statusCode + ' Image: ' + imgHref + ' (' + url + ')'
        DB.updateImage imgHref, res.statusCode, dbid
    req.on 'error', (err) ->
      console.log 'Image request Error:' + err.message


class House extends nodeio.JobClass
  input: linkQueue
  run: (url) ->
    @getHtml url, (err, $, data) =>
      self = this
      if err?
        console.log '1. ' + url + ' - ' + err   
        DB.updatePage url, err  
        @exit
        return false

      dbid = DB.updatePage url, 200
      # The Crawler #
      try 
        selector = if first then $('a[href]') else $('a[href]', $('.content'))
      catch error #generally means there were no links on page
        parsedLinks.push url
        @emit null
        return #console.log '2.' + error.message         
      first = false
      selector.each (a) ->
        href =  getLink a, url
        if href isnt false
          addLink href, self
                
      ### The Parser ###
      # Image Checker #
      try
        images = $('img')
        verifyImages images, url, dbid
      catch error
        #console.log '3. Image retrieval error'     

      # Product Identifier #     
      try 
        item_num = $('#product-item-new').text
        try
          product_name = $('.blue', $('.breadText')).text;
        catch error
          try 
            product_name = $('.product-title').text
          catch error
      catch error
        item_num = null

      if item_num?
        #console.log 'Name: ' + product_name + ' (' + item_num + ')'
        DB.updateProduct product_name, item_num, dbid

      # Cleanup #
      parsedLinks.push url    
      if parsedLinks.length % 100 is 0
        console.log 'Parsed: ' + parsedLinks.length + ' | Stack: ' + linkQueue.length
      #console.log 'Got: ' + url + '\nStack: ' + linkQueue.length + '\nProcessed: ' + parsedLinks.length

      @emit null
  complete: (cb) ->
    cb();
    
          
@class = House
@job = new House({max: 10, timeout: 10, retries: 3})
    
