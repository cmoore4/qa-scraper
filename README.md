QA Web Crawler
===

This project was written for a client ("House", hence the name of some vars) that needed a way to do some QA on their public facing ecommerce site.  The platform they were running on had minimal support for any kind of heirarchical-based reporting (How many sunglasses do we have under $10?).

It also served to do some sanity checking (Are we selling any shoes for over $250?  Under $10?).

Further, it checked for broken links and images, useful for finding links in comment pages to spam, as well as assets from their S3 bucket that served images with the wrong access permissions set.

Node.js was a good solution for this, scraping and parsing pages using jQuery natively, and able to run concurrent http get requests, on the order of 8-10 per second.  A full site scrape of thie 30,000 product site took around 4.5 hours to complete.

Initially written as a one-off, once it saw greater usage I tried my hand at rewriting it in Coffee Script to clean up the ugly spaghetti-code mess the original had turned into.  This greatly enhanced readability of the source, but the project was shelved for unrelated reasons before it was completed.  The source of the re-write is included for academic reasons.
