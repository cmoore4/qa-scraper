(function() {
  var DB, House, addLink, badImages, baseUrl, connection, db, dbVars, errors, first, getLink, http, imageLinks, linkQueue, liveConnections, mysql, nodeio, openImages, parsedLinks, sys, ticks, timer, timex, urlParse, verifyImages;
  var __indexOf = Array.prototype.indexOf || function(item) {
    for (var i = 0, l = this.length; i < l; i++) {
      if (this[i] === item) return i;
    }
    return -1;
  }, __hasProp = Object.prototype.hasOwnProperty, __extends = function(child, parent) {
    for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; }
    function ctor() { this.constructor = child; }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor;
    child.__super__ = parent.prototype;
    return child;
  }, __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
  nodeio = require('node.io');
  urlParse = require('url');
  http = require('http');
  sys = require('sys');
  mysql = require('db-mysql');
  connection = null;
  dbVars = {
    hostname: 'HOST',
    user: 'USER',
    password: 'PASS',
    database: 'QA_DB'
  };
  db = new mysql.Database(dbVars).on('ready', function(server) {
    return connection = this;
  }).connect(function(error) {
    if (error != null) {
      return console.log('Error connecting to Database: ' + error);
    }
  });
  http.Agent.defaultMaxSockets = 50;
  ticks = 0;
  liveConnections = 0;
  errors = 0;
  timer = function() {
    return ticks++;
  };
  timex = setInterval(timer, 1000);
  process.on('uncaughtException', function(err) {
    return console.log('UNCAUGHT ERROR!: ' + err);
  });
  baseUrl = urlParse.parse('http://www.EXAMPLE.com/');
  first = true;
  linkQueue = ['http://www.EXAMPLE.com'];
  parsedLinks = ['http://www.EXAMPLE.com/cgi-bin/cart', 'http://www.EXAMPLE.com/portal/', 'http://www.EXAMPLE.com/team/'];
  badImages = [];
  imageLinks = [];
  openImages = 0;
  addLink = function(href, IOJob) {
    var passed;
    if (__indexOf.call(linkQueue, href) >= 0 || __indexOf.call(parsedLinks, href) >= 0) {
      passed = {
        'new': false,
        href: href
      };
      return passed;
    } else {
      linkQueue.push(href);
      IOJob.add(href);
      passed = {
        'new': true,
        href: href
      };
      return passed;
    }
  };
  getLink = function(a, url) {
    var href, parsedUrl;
    parsedUrl = urlParse.parse(a.attribs.href);
    href = false;
    if (parsedUrl.hasOwnProperty('host')) {
      if (parsedUrl.host === baseUrl.host) {
        href = parsedUrl.href;
      } else {
        href = false;
      }
    } else if (parsedUrl.protocol === 'javascript:') {
      if (url.indexOf('?itemsperpage=10000' !== -1)) {
        href = url + '?itemsperpage=10000';
      } else {
        href = false;
      }
    } else {
      if (url.indexOf('?itemsperpage=10000' !== -1)) {
        href = urlParse.resolve(baseUrl.href, parsedUrl.pathname + (parsedUrl.hasOwnProperty('search') ? parsedUrl.search : ''));
      } else {
        href = false;
      }
    }
    if (href !== false) {
      return href;
    } else {
      return false;
    }
  };
  DB = {
    updatePage: function(url, response) {
      var id, idfn, q;
      id = null;
      idfn = function(err, res) {
        if (err != null) {
          return console.log(err);
        }
        return id = res.id;
      };
      q = connection.query('INSERT INTO Pages(url, `date`, response) VALUES(?, CURDATE(), ?) ON DUPLICATE KEY UPDATE ID=LAST_INSERT_ID(ID), url=?, `date`=CURDATE(), response=?, last_id=NOT last_id', [url, response, url, response]);
      q.execute(idfn, {
        'async': false
      });
      return id;
    },
    updateProduct: function(name, id, price, dbid) {
      var category, itemSplit, year;
      year = null;
      category = null;
      if (id != null) {
        itemSplit = id.split('-');
        year = itemSplit[0].match(/.+?([0-9]{1,2}zz)$/);
        if (year != null) {
          year = year[1].replace('zz', '');
        } else {
          year = null;
        }
        itemSplit.splice(0, 1);
        itemSplit.splice(0, 1);
        category = itemSplit.join(" ");
      }
      return connection.query('INSERT INTO Products(page_id, name, class, item_id, year, price) VALUES(?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE item_id=?, price=?', [dbid, name, category, id, year, price, id, price]).execute(function(err, res) {
        if (err != null) {
          if (err.hasOwnProperty('message')) {
            return console.log('Product DB write failed: ' + err);
          }
        }
      });
    },
    updateImage: function(img, code, dbid) {
      return connection.query('INSERT INTO Images(page_id, url, response) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE response=?', [dbid, img, code, code]).execute(function(err, res) {
        if (err != null) {
          if (err.hasOwnProperty('message')) {
            return console.log('Image DB write failed: ' + err);
          }
        }
      });
    }
  };
  verifyImages = function(images, url, dbid) {
    return images.each(function(img) {
      var imgHref, imgUrl;
      imgUrl = urlParse.parse(img.attribs.src);
      if (!imgUrl.hasOwnProperty('host')) {
        imgUrl.host = baseUrl.host;
      }
      imgHref = imgUrl.host + imgUrl.pathname;
      if (__indexOf.call(imageLinks, imgHref) < 0) {
        imageLinks.push(imgHref);
        DB.updateImage(imgHref, null, dbid);
        return openImages++;
      }
      /*
          openImages++
          req = http.request {method: 'HEAD', host: imgUrl.host, port: 80, path: imgUrl.pathname}, (res) ->      
            res.on 'error', (err) -> 
              console.log 'Image response Error:' + err.message        
            imgHref = imgUrl.host + imgUrl.pathname
            if res.statusCode is 200
              #DB.updateImage imgHref, res.statusCode, dbid
            else
              if imgHref not in badImages
                badImages.push imgHref
                if badImages.length % 10 is 0
                  console.log 'Bad Images: ' + badImages.length
              
            openImages--
          req.on 'error', (err) ->
            console.log 'Image request Error:' + err.message
          req.end
          */
    });
  };
  House = (function() {
    __extends(House, nodeio.JobClass);
    function House() {
      House.__super__.constructor.apply(this, arguments);
    }
    House.prototype.input = linkQueue;
    House.prototype.run = function(url) {
      liveConnections++;
      return this.getHtml(url, __bind(function(err, $, data) {
        var dbid, images, item_num, price, product_name, selector, self;
        self = this;
        if (err != null) {
          DB.updatePage(url, err.toString());
          liveConnections--;
          errors++;
          this.emit(err);
          this.skip;
          this.exit;
          return false;
        }
        dbid = DB.updatePage(url, 200);
        try {
          selector = first ? $('a[href]') : $('a[href]', $('.content'));
        } catch (error) {
          parsedLinks.push(url);
          liveConnections--;
          this.emit(null);
          return;
        }
        first = false;
        selector.each(function(a) {
          var href;
          href = getLink(a, url);
          if (href !== false) {
            return addLink(href, self);
          }
        });
        /* The Parser */
        try {
          images = $('img');
          verifyImages(images, url, dbid);
        } catch (error) {

        }
        try {
          item_num = $('#product-item-new').text;
          try {
            product_name = $('.product-title').text;
            if (product_name === '') {
              try {
                product_name = $('.blue', $('.breadText')).text;
              } catch (error) {
                product_name = null;
              }
            }
          } catch (error) {
            try {
              product_name = $('.blue', $('.breadText')).text;
            } catch (error) {
              product_name = null;
            }
          }
          try {
            price = parseFloat($('#product-price-house1').text.replace('$', ''));
          } catch (error) {
            price = null;
          }
        } catch (error) {
          item_num = null;
          product_name = null;
          price = null;
        }
        if (item_num != null) {
          DB.updateProduct(product_name, item_num, price, dbid);
        }
        parsedLinks.push(url);
        liveConnections--;
        this.emit(product_name != null ? product_name : url);
        return true;
      }, this));
    };
    House.prototype.complete = function(cb) {
      console.log('Finished crawling.');
      clearInterval(timex);
      return cb();
    };
    House.prototype.output = function(out) {
      return console.log(parsedLinks.length + '\t| ' + linkQueue.length + '\t| ' + liveConnections + '\t| ' + errors + '\t|' + ticks + '\t|' + openImages + '\t|' + out[0]);
    };
    House.prototype.fail = function(input, status) {
      console.log('Failed thread: ' + input + ' with status ' + status);
      liveConnections--;
      return this.emit(null);
    };
    return House;
  })();
  this["class"] = House;
  this.job = new House({
    max: 15,
    timeout: 15,
    retries: 3
  });
}).call(this);
